//
//  WeatherImage.swift
//  WeatherApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit
/*=================================================*/
// Set Weather Image
//// Get Weather Id Frome json["weather"][id]
//// Get Crente Time (Am || Pm ) from
//// Prepare Image  and return to ViewController
/*=================================================*/
struct SetWeatherImage {
    
    static func loadWeatherId (GetWeatherId WeatherId:Int, GetAmPmTime AmPm:String ) -> String
    {
        var WeatherImage:String
        switch WeatherId
        {
        case 200 , 201 , 202 , 210 , 211 , 212 , 221 , 230 , 231 , 232 : // Thunderstorm
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp06"
            } // Day
            else
            {
                WeatherImage = "WeatherApp07"
            } // Night
            
        case 300 , 301 , 302 , 310 , 311 , 312 , 313 , 314 , 321 :      // Drizzle
            
            WeatherImage = "WeatherApp14"// Day
            
        case 500, 501, 502, 503, 504, 511, 520, 521, 522, 531 :         // Rain
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp23"
            }// Day
            else
            {
                WeatherImage = "WeatherApp24"
            } // Night
            
        case 600, 601, 602, 611, 612, 615, 616, 620, 621, 622 :         // Snow
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp09"
            } // Day
            else
            {
                WeatherImage = "WeatherApp10"
            } // Night
            
        case 701, 711, 721, 731, 741, 751, 761, 762, 771, 781 :         // Atmosphere
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp35"
            } // Day
            else {
                WeatherImage = "WeatherApp35"
            } //Night
            
        case 800 :                                                      // Clear
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp01"
            } //Day
            else
            {
                WeatherImage = "WeatherApp02"
            } //Night
            
        case 801 :                                                      // few clouds
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp19" }     //Day
            else
            {
                WeatherImage = "WeatherApp20"
            } //Night
            
        case 802 :                                                      // scattered clouds
            WeatherImage = "WeatherApp18"           //Day
            
        case 803 , 804 :                                                // broken clouds
            WeatherImage = "WeatherApp18"
            
        case 900, 901, 902 :                                            // tornado
            if AmPm == "AM"
            {
                WeatherImage = "WeatherApp48"
            } //Day
            else
            {
                WeatherImage = "WeatherApp48"
            } //Night
            
        default:
            WeatherImage = "WeatherApp38"
            
        }
        
        return WeatherImage
    }
    
}
