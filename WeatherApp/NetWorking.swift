//
//  NetWorking.swift
//  WeatherApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//
import Foundation

class NetWorking {
    
    // (WeatherId:Int,SunRaise:String,AmPm:String,temp:Int,humidity:Int,windSpeed:Double,CityName:String,date:String)
    
    
    func loudData(CityName city:String, completionHandler: @escaping (_ temp:Int,_ humidity:Int,_ WeatherId:Int,_ SunRaise:Int,_ CityName:String,_ windSpeed:Double,_ serverTime:Int,_ Response:Int) -> Void)
    {
        
        var temp: Int?
        var humidity:Int?
        var WeatherId:Int?
        var SunRaise:Int?
        var CityName:String?
        var windSpeed:Double?
        var serverTime:Int?
        var Response:Int?
        
        
        
        
        
        
        if let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=ce02b80d237a93988496e5aee074247c"){
            
            URLSession.shared.dataTask(with: url){  (data, response, error) in
                
                if let httpResponse = response as? HTTPURLResponse {
                    print(httpResponse.statusCode)
                    Response = httpResponse.statusCode
                }
                
                if error != nil
                {
                    print("error")
                }
                    
                else
                {
                    if let myData = data
                    {
                        do{
                            let json = try JSONSerialization.jsonObject(with: myData) as! NSDictionary
                            if let main = json["main"] as? [String:Any]{
                                
                                temp = (main["temp"] as? Int)!
                                humidity = (main["humidity"] as? Int)!
                                let wind = json["wind"] as! [String:Any]
                                windSpeed = (wind["speed"] as? Double)!
                                let sys = json["sys"] as! [String:Any]
                                //Date
                                SunRaise = sys["sunrise"] as? Int
                                CityName = (json["name"] as? String)!
                                serverTime = json["dt"] as! Int
                                // Description Id
                                let weathers = json["weather"] as! [[String:Any]]
                                for weather in weathers
                                {
                                    WeatherId = (weather["id"] as? Int)!
                                    
                                }
                                
                                
                                    
                                
    completionHandler(temp!,humidity!,WeatherId!,SunRaise!,CityName!,windSpeed!,serverTime!,Response!)
                                    
                                UserDefaults.standard.set(json, forKey: "JsonOFLine")
                            }
                        } catch{ print ("error")}
                    }
                }
                }.resume()
        }
    }
    
    class func loudDataOfline(completionHandler: @escaping (_ temp:Int,_ humidity:Int,_ WeatherId:Int,_ SunRaise:Int,_ CityName:String,_ serverTime:Int,_ windSpeed:Double) -> Void)
    {
        var temp: Int?
        var humidity:Int?
        var WeatherId:Int?
        var SunRaise:Int?
        var CityName:String?
        var windSpeed:Double?
        var serverTime:Int?
    
        
        let json = UserDefaults.standard.value(forKeyPath: "JsonOFLine") as! NSDictionary
        if let main = json["main"] as? [String:Any]{
            
            temp = (main["temp"] as? Int)!
            humidity = (main["humidity"] as? Int)!
            let wind = json["wind"] as! [String:Any]
            windSpeed = (wind["speed"] as? Double)!
            let sys = json["sys"] as! [String:Any]
            //Date
            SunRaise = sys["sunrise"] as? Int
            CityName = (json["name"] as? String)!
            serverTime = json["dt"] as! Int
            
            // Description Id
            let weathers = json["weather"] as! [[String:Any]]
            for weather in weathers
            {
                WeatherId = (weather["id"] as? Int)!
                
            }
            
            
            completionHandler(temp!,humidity!,WeatherId!,SunRaise!,CityName!,serverTime!,windSpeed!)
    
    }
    
    
    
    }
    
}
