//
//  TimeStamp.swift
//  WeatherApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation

struct timeStamp {
    
    static func timeStamp(Formattime format:String,TimeAndDate timeAndDate:Int ) -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(timeAndDate))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let returnDate = dateFormatter.string(from: date)
        
        return returnDate
    }
    
    static func CurentDate (Formattime format:String ) -> String
    {
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let returnDate = dateFormatter.string(from: date as Date)
        
        return returnDate
    }
    
}
