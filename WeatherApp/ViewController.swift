//
//  ViewController.swift
//  WeatherApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import UIKit


class ViewController: UIViewController,ErrorFeedBack {
    
    @IBOutlet weak var textCity: UITextField!
    @IBOutlet weak var MenuView: UIView!
    @IBOutlet weak var ConstrantLeft: NSLayoutConstraint!
    @IBOutlet weak var LaCityName: UILabel!
    @IBOutlet weak var LaDate: UILabel!
    @IBOutlet weak var UiImageWeather: UIImageView!
    @IBOutlet weak var LaTempDegre: UILabel!
    @IBOutlet weak var LaHumidity: UILabel!
    @IBOutlet weak var LaWind: UILabel!
    @IBOutlet weak var LaSunRise: UILabel!
    @IBOutlet weak var formatS: UISegmentedControl!
    var city:String = UserDefaults.standard.object(forKey: "CityName") as!String ?? "Cairo"
    /////////////////////
    let data:NetWorking     = NetWorking()
    var temp:Int            = 0
    var humidity:Int        = 0
    var WeatherId:Int       = 0
    var SunRaise:Int        = 0
    var CityName:String     = "Cairo"
    var WindSpeed:Double    = 0.0
    var serverTime:Int      = 0
    var AmPm:String?
    var menuShowing         = false
    var StatesCode:Int?
    
    // var loudDautaIns = loudData.loudData(CityName: city)
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        MenuView.layer.shadowOpacity = 1
        MenuView.layer.shadowRadius = 6
        self.loudFormatUnit()
        GetData()
        SetData()
        
        
    }
    
    func GetData(){
        
        if !(CheckConnection.Connection()){
            
            DispatchQueue.main.async{
                NetWorking.loudDataOfline(completionHandler:{Temp,Humidity,WindSpeed,SunRaise,CityName,WeatherId,Dt
                    in
                    
                    
                    self.temp                = Temp
                    self.humidity            = Humidity
                    self.WeatherId           = WeatherId
                    self.SunRaise            = SunRaise
                    self.CityName            = CityName
                    self.WindSpeed           = Double(WindSpeed)
                    self.serverTime          = Int(Dt)
                    
                    self.SetData()
                    
                })
            }}
        
        
        DispatchQueue.main.async{
            self.data.loudData(CityName: self.city, completionHandler:{Temp,Humidity,WindSpeed,SunRaise,CityName,WeatherId,Dt,Response
                in
                
                if Response == 404{
                    DispatchQueue.main.sync
                        {
                            
                            self.showErrorAlert(title: "City Not found", msg: "Enter Valid City Name")
                    }
                    return
                }
                else
                {
                    self.temp                = Temp
                    self.humidity            = Humidity
                    self.WeatherId           = Int(WeatherId)
                    self.SunRaise            = SunRaise
                    self.CityName            = CityName
                    self.WindSpeed           = Double(WindSpeed)
                    self.serverTime          = Dt
                    self.SetData()
                }
                
            })
        }
        
    }
    
    @IBAction func openMenu(_ sender: Any)
    {
        
        UIView.animate(withDuration: 0.5, animations:{
            if (self.menuShowing)
            {
                self.ConstrantLeft.constant = -150
                
            }
            else
            {
                self.ConstrantLeft.constant = 0
                UIView.animate(withDuration: 0.3, animations:{
                    
                    self.view.layoutIfNeeded()
                    
                })
            }
            self.menuShowing = !self.menuShowing
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetData()
    {
        DispatchQueue.main.async {
            
            //Date
            UIView.animate(withDuration: 0.5, animations: {
                self.LaSunRise.text = timeStamp.timeStamp(Formattime: "hh:mm aa", TimeAndDate: self.SunRaise)
                self.LaDate.text = timeStamp.timeStamp(Formattime: "YYYY-MMM-dd", TimeAndDate: self.serverTime)
                
                self.LaCityName.text = self.CityName
                self.LaHumidity.text = "\(String(describing: self.humidity))%"
                self.LaWind.text = "\(String(describing: self.WindSpeed)).Km"
                
                // Description Id
                // bug by Server Time in ImageId & Date
                self.AmPm = timeStamp.timeStamp(Formattime: "a", TimeAndDate: self.serverTime)
            })
            let ImageId = SetWeatherImage.loadWeatherId(GetWeatherId: self.WeatherId, GetAmPmTime: self.AmPm!)
            
            UIView.transition(with: self.UiImageWeather, duration: 0.5, options: .transitionFlipFromLeft, animations:{
                
                self.UiImageWeather.image = UIImage(named: ImageId)
                
            }, completion: nil)
            self.loudFormatUnit()
            
        }
    }
    
    
    
}
