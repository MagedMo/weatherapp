//
//  formatFunc.swift
//  WeatherApp
//
//  Created by Magid on 10/10/18.
//  Copyright © 2018 Magid. All rights reserved.
//

import Foundation
import UIKit

extension ViewController
{
    
    @IBAction func FormarAction(_ sender: Any)
    {
        switch formatS.selectedSegmentIndex
        {
        case 0:
            UserDefaults.standard.set("metric", forKey: "format")
            
            loudFormatUnit()
        case 1:
            UserDefaults.standard.set("imperial", forKey: "format")
            loudFormatUnit()
        case 2:
            UserDefaults.standard.set("f", forKey: "format")
            self.LaTempDegre.text = "\(String(describing: self.temp))º"
        default:
            UserDefaults.standard.set("f", forKey: "format")
            self.LaTempDegre.text = "\(String(describing: self.temp))º"
        }
    }
    
    
    func loudFormatUnit()
    {
        if UserDefaults.standard.string(forKey: "format") == "metric"
        {
            formatS.selectedSegmentIndex = 0
            let Unit = temp - 273
            self.LaTempDegre.text = "\(String(describing: Unit))º"
        }else if UserDefaults.standard.string(forKey: "format") == "imperial"
        {
            formatS.selectedSegmentIndex = 1
            let Unit = (Double((temp - 273))*1.8)+32
            self.LaTempDegre.text = "\(String(describing: Unit))º"
        } else
        {
            formatS.selectedSegmentIndex = 2
            self.LaTempDegre.text = "\(String(describing: self.temp))º"
        }
        
    }
    
    
    
}
